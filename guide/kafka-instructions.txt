
### STEP ONBOARD OF THE DOCKER ###
docker run --rm -it landoop/fast-data-dev:1.0.1 bash

### CREATE KAFKA TOPICS ###
/usr/local/bin/kafka-topics --create --zookeeper <address>:2181 --replication-factor 1 --partitions 1 --topic stream-dataset

/usr/local/bin/kafka-topics --create --zookeeper <address>:2181 --replication-factor 1 --partitions 1 --topic stream-cleaned-data

/usr/local/bin/kafka-topics --create --zookeeper <address>:2181 --replication-factor 1 --partitions 1 --topic stream-discarded-data


