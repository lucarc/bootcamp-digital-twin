#!/usr/bin/env bash

mkdir -p /var/tmp/mqtt-kafka-docker &>/dev/null
docker rm -f mqtt-kafka-docker &>/dev/null

docker run -e DISABLE_JMX=1 -e RUNTESTS=0 -e CONNECT_HEAP=1G --net=host -v /var/tmp/mqtt-kafka-docker:/tmp --name=mqtt-kafka-docker landoop/example-mqtt
