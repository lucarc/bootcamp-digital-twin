#include <SimpleDHT.h>

// for DHT11, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: 2
int pinDHT11 = 2;
SimpleDHT11 dht11;

// for FC-28, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: A1, A2
int sensor_pin_pot1 = A2;
int output_value_pot1;
int sensor_pin_pot2 = A3;
int output_value_pot2;

// for Light Sesnsor, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: A5
int sensor_pin_light = A5;
int output_value_light;

void setup() {
  Serial.begin(9600);
}

void loop() {

  // DHT11: AIR HUMIDITY AND TEMPERATURE
  // read with raw sample data.
  byte temperature = 0;
  byte humidity = 0;
  byte data[40] = {0};

  if (dht11.read(pinDHT11, &temperature, &humidity, data)) {
    //Serial.println("Read DHT11 failed");
    return;
  }

  // FC-28: MOISTURE
  output_value_pot1 = analogRead(sensor_pin_pot1);
  output_value_pot2 = analogRead(sensor_pin_pot2);

  // We map the analog output values from the soil moisture sensor FC-28 to 0-100,
  // because the moisture is measured in percentage.
  // When we took the readings from the dry soil the sensor value was XXX,
  // and from the wet soil the sensor value was YYY.
  // So, we mapped these values to get the moisture.
  
  // How to derive the values of XXX and YYY?

  output_value_pot1 = map(output_value_pot1,0,150,0,100);
  output_value_pot2 = map(output_value_pot2,0,250,0,100);

  // LIGHT SENSOR
  output_value_light = analogRead(sensor_pin_light);
  output_value_light = map(output_value_light,1023,63,0,436);

  Serial.print("{ ");
  
  Serial.print("\"humidity_air\":");
  Serial.print((int)humidity);
  
  Serial.print(",\"temperature_air\":");
  Serial.print((int)temperature);
  
  Serial.print(",\"moisture_pot_1\":");
  Serial.print(output_value_pot1);

  Serial.print(",\"moisture_pot_2\":");
  Serial.print(output_value_pot2);

  Serial.print(",\"illuminance\":");
  Serial.print(output_value_light);
  
  Serial.println(" }");

  // Greenhouse sampling rate is 1HZ.
  delay(1000);
}
