from __future__ import print_function
from time import sleep
import paho.mqtt.client as mqtt
import glob
import serial
from serial.tools import list_ports

'''
Define all the variables needed to establish a connection to the Arduino and to the MQTT Broker:

    - The Arduino is connected through a USB port to the PC, thus we need to check which device as been assigned 
      to it (variable arduino_port)

    - The MQTT Broker is running inside a Docker, which is sharing the network with the host (PC)

'''

mqtt_broker_address = "localhost"

mqtt_topic = "arduino/data"

for port in list_ports.comports():
    print(port)

device = glob.glob("/dev/tty*")[0]

# Open a connection through the serial port
try:
    arduino = serial.Serial(device, 4800, timeout=15)
except Exception as ex:
    template = "An exception of type {0} occurred. Arguments:\n{1!r}"
    message = template.format(type(ex).__name__, ex.args)
    print(message)
    print("Please check the port!")


'''
Setup of the MQTT Client:

   - We need to define an "on_publish" method, which is called when a message that was sent using the publish() call has
     completed transmission to the MQTT broker

   - We need to establish a connection with the MQTT Broker through a Client instance

   - Always consider that we are receiving plain text from the Arduino
'''


def on_publish(client, userdata, mid):
    print("Publishing message {} to topic '{}':".format(mid, mqtt_topic))
    client.on_publish = on_publish


# Create a new MQTT Client instance assigning a client ID
client = mqtt.Client("Producer_1")
print("Connecting to MQTT Broker")
client.connect(mqtt_broker_address, 1883)

#################################################################


publish_rate = 1  # The same as the one set on the Arduino
while True:
    sample = str(arduino.readline().decode('utf-8'))
    client.publish(mqtt_topic, sample)

    print("Publishing message to topic {} message:\n{}".format(mqtt_topic, sample))
    sleep(publish_rate)
