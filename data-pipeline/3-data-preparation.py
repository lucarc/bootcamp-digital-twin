from confluent_kafka import Producer, Consumer, KafkaError, KafkaException
import sys
import json

verbosity = False
# Adding Producer Configurations
PRODUCER_CONFIG = {
    'bootstrap.servers': '35.232.151.71:9092',
    'acks': 1
}

# Adding Consumer Configurations
CONSUMER_CONFIG = {
    'bootstrap.servers': '35.232.151.71:9092',
    'enable.auto.commit': 'true',
    'group.id': 'grp_1',
    'default.topic.config': {
        'auto.offset.reset': 'earliest'
    }
}

# Define all the topics
topic_consumer = "stream-dataset"
topic_producer = "stream-cleaned-data"
topic_discard = "stream-discarded-data"

# Create the consumer and subscribe to topics list
c = Consumer(CONSUMER_CONFIG)
consumer_topics = [topic_consumer]
c.subscribe(consumer_topics)

# Create the producers
p = Producer(PRODUCER_CONFIG)
p_discarded = Producer(
    PRODUCER_CONFIG)  # Method to verify the correctness of the publishing by the PRODUCERS (callback function)


# Callback function
def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


'''
Consume from Kafka, check the correctness of the data (clean them!):

- We need to consume from the Kafka Topic where the data coming from the sensors have been published

- Since a Consumer is always consuming, it can reach the end of the queue. We need to check if the consumed message 
  is empty or not

- When we have consumed some data, we need to check if the values are compliant with what we expect, the measures 
  coming from the sensors can encompass noise

- We will send all the data that we do not consider "clean" to a so-colled "Dead Letter Queue". 
  This is just another topic where we will store all the messages considered "dirty"
'''

try:
    #### CONSUMER ####
    while True:

        msg = c.poll(timeout=1.0)
        if msg is None:
            continue
        if msg.error():
            # Error or event
            if msg.error().code() == KafkaError._PARTITION_EOF:
                # End of partition event
                if verbosity:
                    sys.stderr.write('%% %s [%d] reached end at offset %d\n' %
                                     (msg.topic(), msg.partition(), msg.offset()))
            else:
                # Error
                raise KafkaException(msg.error())

        else:
            # Proper message
            print('%% {} [{}] at offset {} with key {}:'.format(msg.topic(), msg.partition(), msg.offset(),
                                                                str(msg.key())))
            print("{}\n".format(msg.value()))

            value_string = msg.value().decode('utf-8')

            # < load the data and prepare to send them >

            if value["temperature_air"] < 10 or value["humidity_air"] < 0 or value["moisture_pot_1"] < 0 or value[
                "moisture_pot_2"] < 0 or value["illuminance"]<0:

                # <produce the data to topic_discard> #
            else:

                # <produce the data to topic_cleaned> #

except Exception as e:
    print("Unexpected error: {}".format(sys.exc_info()[0]))
    raise
