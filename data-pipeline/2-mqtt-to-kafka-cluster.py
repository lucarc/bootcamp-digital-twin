from __future__ import print_function
import paho.mqtt.client as mqtt
from confluent_kafka import Producer
import json

'''
Define all the configurations needed by a Kafka Producer to connect to the Kafka Cluster
'''
PRODUCER_CONFIG = {
    'bootstrap.servers': '35.232.151.71:9092',
    'acks': 1
}

p = Producer(PRODUCER_CONFIG)

topic_name = "stream-dataset"

mqtt_broker_address = "localhost"

'''
Define a callback function:

The next method define a callback function that provide asynchronous handling of request completion. 
This method will be called when the record sent to the server has been acknowledged, giving us the possibility 
to understand if the message was correctly delivered or not.
'''
# Method to verify the correctness of the publishing by the PRODUCERS (callback function)
def delivery_report(err, msg):
    verbosity = True
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        if verbosity:
            print('#-> Message delivered to data-pipeline topic {} [{}]\n'.format(msg.topic(), msg.partition()))

'''
Setup of the MQTT Client:

- We need to define an "on_message" method, which is called when a message has been received on a topic that 
the client subscribes to. In this case, whenever a message has been received, it will be sent to a Kafka topic

- We need to establish a connection with the MQTT Broker through a Client instance
'''
def on_message(client, userdata, message):
    try:
        data = json.loads(message.payload.decode("utf-8"))
        print("Retrieving from MQTT topic '{}' message:".format(message.topic))
        print("{}\n".format(data))
    except ValueError as e:
        print('ERROR - Invalid json: %s' % e)
        raise

    data = json.dumps(data).encode('ascii')
    p.poll(0)
    # Asynchronously produce a message, the delivery report callback
    # will be triggered from poll() above, or flush() below, when the message has
    # been successfully delivered or failed permanently.
    p.produce(topic_name, data, "raw-data", callback=delivery_report)

    # Wait for any outstanding messages to be delivered and delivery report
    # callbacks to be triggered.
    p.flush(10)


'''
Create a new MQTT Client that needs to "subscribe" to the same topic where the MQTT Producer is publishing the data
'''
print("Creating a new MQTT Client instance")
client = mqtt.Client("Consumer_1")

# Attach a function to callback
# -----------------------------#

print("Connecting to the MQTT Broker")
# -----------------------------#

print("Subscribing to topic", "arduino/data\n")
# -----------------------------#

# Forever waiting for new messages
client.loop_forever()
